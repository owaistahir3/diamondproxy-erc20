// SPDX-License-Identifier: MIT
pragma solidity >=0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./ERC20Storage.sol";
import "./ERC20Lib.sol";

contract ERC20Facet is ERC20 {

    constructor(string memory _name, string memory _symbol, uint256 _intialsupply) ERC20(_name, _symbol) {
        ERC20StorageLib.ERC20Storage storage es = ERC20StorageLib.erc20Storage();
        //es.unitsOneEthCanBuy = 10;
        //es.minter = msg.sender;
        es.name = _name;
        es.symbol = _symbol;
        es.intialsupply = _intialsupply;
        es.tokenOwner = msg.sender;
        es.balances[msg.sender] = es.intialsupply;

        ERC20Lib.mint(msg.sender, _intialsupply);
    }

    function name() public view override returns (string memory) {

        return ERC20StorageLib.erc20Storage().name;
    }

    function setName(string calldata _name) external {
        ERC20StorageLib.erc20Storage().name = _name;
    }

    function symbol() public view override returns (string memory) {
        return ERC20StorageLib.erc20Storage().symbol;
    }

    function setSymbol(string calldata _symbol) external {
        ERC20StorageLib.erc20Storage().symbol = _symbol;
    }

    function mint(address receiver, uint amount) external {
            ERC20Lib.mint(receiver, amount);
    }
    
    function burn(address burner, uint _value) external returns (bool){
        bool success = ERC20Lib.burn(burner, _value);
        return success;
    } 

    function balanceOf(address _of) public view override returns (uint256) {

        return ERC20StorageLib.erc20Storage().balances[_of];
    }

    function transfer(address _to, uint256 _amount) public override returns (bool) {
        _transfer(msg.sender, _to, _amount);
        return true;
    }

    function _transfer( address _from, address _to, uint256 _amount) internal override {
        ERC20StorageLib.ERC20Storage storage es = ERC20StorageLib.erc20Storage();

        es.balances[_from] -=_amount;
        es.balances[_to] += _amount;

        emit Transfer(_from, _to, _amount);
    }
}



