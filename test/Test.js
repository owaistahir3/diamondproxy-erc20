const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("ERC20Facet contract", function () {
  it("Deployment should mint tokens", async function () {
   
    const [deployer] = await hre.ethers.getSigners();

    console.log(
        "Deploying contracts with the account:",
        deployer.address
    );

    console.log("Account balance:", (await deployer.getBalance()).toString());

    const Token = await hre.ethers.getContractFactory("ERC20Facet");
    const token = await Token.deploy("ACE Token","ACE",1000);

    const minto = await token.mint(es.minter.address,1000)

    console.log("Token address", token.address);
  });
});