// SPDX-License-Identifier: MIT
pragma solidity >=0.8.9;

import "./ERC20Storage.sol";

library ERC20Lib {
  
  event Transfer(address indexed from, address indexed to, uint256 amount);

    function mint(address receiver, uint amount) internal {
            ERC20StorageLib.ERC20Storage storage es = ERC20StorageLib.erc20Storage();
            require(msg.sender == es.minter);
            require(amount < 1e60);
            es.balances[receiver] += amount;
        }
    
    function burn(address burner, uint _value) internal returns (bool success) {
        ERC20StorageLib.ERC20Storage storage es = ERC20StorageLib.erc20Storage();
        require(es.balances[burner] >= _value);
        
        es.balances[burner] -= _value;
        es.intialsupply -= _value;
        emit Transfer(burner, address(0), _value);    

        return true;
    }
}