// SPDX-License-Identifier: MIT
pragma solidity >=0.8.9;

library ERC20StorageLib {
     bytes32 constant ERC_20_STORAGE_POSITION = keccak256(
    "PCToken.storage.location"
  );

    struct ERC20Storage {
        string name;
        string symbol;
        uint256 intialsupply;
        address tokenOwner;
        address minter;
        uint256 unitsOneEthCanBuy;
        mapping(address => uint256) balances;
    }

    function erc20Storage() internal pure returns (ERC20Storage storage es) {
        bytes32 position = ERC_20_STORAGE_POSITION;
        assembly {
            es.slot := position
        }
    }

}